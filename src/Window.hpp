#pragma once

#include <memory>

#include <QMainWindow>
#include <QMenuBar>
#include <QtMultimedia/QAudioOutput>
#include <QFile>

#include "Current.hpp"

class QAction;

class Window final: public QMainWindow
{
	Q_OBJECT
public:
	explicit Window();

private Q_SLOTS:
	void openFile();

private:
	//QAudioOutput *const qAudioOutput;

	Current* current_;

	QAction* const aPlay;
	QAction* const aPause;
	QAction* const aStop;
};
