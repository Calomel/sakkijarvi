#include "Current.hpp"

#include <memory>
#include <iostream>

#include <QAudioDecoder>

#include "util.hpp"

namespace
{

/**
 * Return the zero point of a given type
 */
template <typename T> constexpr T
typeZero()
{
	if (std::numeric_limits<T>::is_signed)
		return 0;
	else
		return std::numeric_limits<T>::max() / 2 + 1; // round up
}

template <typename T> void
pushSamplesT(std::vector<real>& out,
		int const nChannels, uint8_t const* const data, size_t const size)
{
	using Signed = typename std::make_signed<T>::type;

	auto dataT = (T const* const) data;
	auto dataEnd = (T const* const) (data + size);
	real const scale = 1 / (real) std::numeric_limits<Signed>::max();
	while (dataT < dataEnd)
	{
		out.push_back(Signed(*dataT - typeZero<T>()) * scale);
		dataT += nChannels;
	}
}
template <> void
pushSamplesT<float>(std::vector<real>& out,
		int const nChannels, uint8_t const* const data, size_t const size)
{
	using T = float;
	auto dataT = (T const* const) data;
	auto dataEnd = (T const* const) (data + size);
	while (dataT < dataEnd)
	{
		out.push_back(real(*dataT));
		dataT += nChannels;
	}
}
template <> void
pushSamplesT<double>(std::vector<real>& out,
		int const nChannels, uint8_t const* const data, size_t const size)
{
	using T = double;
	auto dataT = (T const* const) data;
	auto dataEnd = (T const* const) (data + size);
	while (dataT < dataEnd)
	{
		out.push_back(real(*dataT));
		dataT += nChannels;
	}
}


void pushSamples(std::vector<float>& out, QAudioBuffer& buf)
{
	// buf.format().isValid() == true
	//
	int const nChannels = buf.format().channelCount();

#define TYPE_CASE(type) \
		pushSamplesT<type>(out, nChannels, \
			(uint8_t*) buf.constData(), buf.byteCount()); \
	/* end of macro */

	switch (buf.format().sampleType())
	{
	case QAudioFormat::SignedInt:
		switch (buf.format().bytesPerFrame())
		{
		case 1: TYPE_CASE(int8_t); return;
		case 2: TYPE_CASE(int16_t); return;
		case 4: TYPE_CASE(int32_t); return;
		case 8: TYPE_CASE(int64_t); return;
		default: goto fail;
		}
	case QAudioFormat::UnSignedInt:
		switch (buf.format().bytesPerFrame())
		{
		case 1: TYPE_CASE(uint8_t); return;
		case 2: TYPE_CASE(uint16_t); return;
		case 4: TYPE_CASE(uint32_t); return;
		case 8: TYPE_CASE(uint64_t); return;
		default: goto fail;
		}
	case QAudioFormat::Float:
		switch (buf.format().bytesPerFrame())
		{
		case 4: TYPE_CASE(float); return;
		case 8: TYPE_CASE(double); return;
		}
	default:
		goto fail;
	}
fail:;
	// Reached invalid format
}

} // namespace anonymous





/// class Current ///

Current::Current(QString const& filename, QObject* const parent)
	: QObject(parent)
	, decoder(new QAudioDecoder(this))
{
	decoder->setSourceFilename(filename);
	duration_ = decoder->duration();

	connect(decoder, &QAudioDecoder::finished,
	        this, &Current::onFinish);
	connect(decoder, QOverload<QAudioDecoder::Error>::of(&QAudioDecoder::error),
	        this, &Current::onError);
	connect(decoder, &QAudioDecoder::bufferReady,
	        this, &Current::onBufferReady);

}

void Current::start()
{
	std::cerr << "Starting decoder\n";
	decoder->start();
}

void Current::onError(QAudioDecoder::Error error)
{
	(void) error;
	std::cerr << "[Decoder] Error: " << decoder->error() << '\n';
	finished_ = true;
}
void Current::onFinish()
{
	std::cerr << "[Decoder] Decoding complete. "
            << "#Packets=" << nDecodedPackets
            << "#Samples=" << nSamples
	          << "\n";
	finished_ = true;
}

void Current::onBufferReady()
{
	QAudioBuffer buffer = decoder->read();
#if 0
	std::cerr << "Decoding format: " << buffer.format().sampleType()
	          << ", Width: " << buffer.format().bytesPerFrame()
	          << ", Channels: " << buffer.format().channelCount()
		<< '\n';
#endif
	std::vector<real> samples;
	pushSamples(samples, buffer);

	// avg
	real avg = 0.0;
	for (real x: samples)
	{
		avg += x;
	}
	avg /= samples.size();
	std::cerr << "Read samples: " << samples.size() << ", avg" << avg << '\n';

	++nDecodedPackets;
	nSamples += samples.size();
}
