#include "Window.hpp"

#include <iostream>

#include <QStringList>
#include <QFileDialog>
#include <QToolBar>
#include <QStyle>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QAudioDecoder>


#define ADD_MENU(var, name) \
		QMenu* const var = menuBar()->addMenu(tr(name))
#define ADD_ACTION(var, name, menu) \
		QAction* var = new QAction(tr(name)); \
		menu->addAction(var); \
	/* end of macro */

Window::Window()
	: QMainWindow(nullptr)
	, current_(nullptr)
	, aPlay(new QAction(
			style()->standardIcon(QStyle::SP_MediaPlay),
			tr("Play"), this))
	, aPause(new QAction(
			style()->standardIcon(QStyle::SP_MediaPause),
			tr("Pause"), this))
	, aStop(new QAction(
			style()->standardIcon(QStyle::SP_MediaStop),
			tr("Stop"), this))
{
	{
		ADD_MENU(menuFile, "File");
		ADD_ACTION(aOpen, "Open", menuFile);

		connect(aOpen, &QAction::triggered, this, &Window::openFile);
	}
	{
		ADD_MENU(menuEdit, "Edit");
		ADD_ACTION(aOptions, "Options", menuEdit);
	}
	{
		ADD_MENU(menuExec, "Execute");
		ADD_ACTION(aAnalyse, "Analyse", menuExec);
	}

	auto* const widgetCentral = new QWidget();
	this->setCentralWidget(widgetCentral);

	QHBoxLayout* const layoutMain = new QHBoxLayout();
	widgetCentral->setLayout(layoutMain);


	// Toolbar
	auto* const toolBar = new QToolBar(this);
	layoutMain->addWidget(toolBar);
	{
		aPlay->setDisabled( true );
		connect(aPlay, &QAction::triggered,
		        this, [this]()
			{
				current_->start();
			});

		toolBar->addAction(aPlay);
		toolBar->addAction(aPause);
		toolBar->addAction(aStop);
	}
}

void Window::openFile()
{
	QString const filename = QFileDialog::getOpenFileName(this, "~");

	delete current_;
	current_ = new Current(filename, this);

	aPlay->setDisabled(false);

	std::cout << "Read audio file of"
	          << " duration=" << current_->duration()
	          << std::endl;
}
